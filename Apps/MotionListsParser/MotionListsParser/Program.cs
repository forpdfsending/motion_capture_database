﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace MotionListsParser
{
    class Program
    {

        MySqlConnection connection;

        string CategoryID = "";

        static void Main(string[] args)
        {

            Program p = new Program();
            p.StartMe();

        }

        [STAThread]
        public void StartMe()
        {
            string server = "srv1.lifesim.biz";
            string database = "yii2basic";
            string uid = "geek";
            string password = "Geekkeeg_156+";
            string connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
            connection = new MySqlConnection(connectionString);

            //processList();


            string initialDir = "c:/Art/Projects/motion_capture_database/";

            if (!initialDir.Equals(""))
            {
                OpenConnection();
                recursiveDirProcessor(initialDir);
            }
        }

        public void processList()
        {
            string[] lines = File.ReadAllLines(@"c:\Art\Projects\motion_capture_database\Info\cmu-mocap-index-text.txt");

            foreach (string line in lines)
            {
                if (line.StartsWith("Subject #"))
                {
                    Regex regex = new Regex(@"(.*)\#(\d+)( )(\(.*\))");
                    //Match match = regex.Match("Subject #56 (vignettes - locomotion, upper-body motions (focus: motion transitions))");
                    Match match = regex.Match(line);
                    if (match.Success)
                    {
                        //for (int x = 1; x <= match.Groups.Count; x++)
                        //{
                        //    Console.Write(match.Groups[x] + " *** ");
                        //}
                        //Console.WriteLine("");

                        CategoryID = match.Groups[2].ToString();
                        string CategoryName = match.Groups[4].ToString();

                        if (CategoryName.StartsWith("("))
                        {
                            CategoryName = CategoryName.Substring(1);
                        }
                        if (CategoryName.EndsWith(")"))
                        {
                            CategoryName = CategoryName.Substring(0, CategoryName.LastIndexOf(")"));
                        }

                        InsertBvhCategory(CategoryName, CategoryID);
                    }
                }
                else if (line.Length > 3)
                {
                    Regex regex = new Regex(@"(\w+)(\t)(.*)");
                    Match match = regex.Match(line);
                    if (match.Success)
                    {
                        string BvhCategoryId = CategoryID;
                        string BvhName = match.Groups[1].ToString();
                        string BvhDescr = match.Groups[3].ToString();
                        InsertBvhFile(BvhCategoryId, BvhName, BvhDescr);
                    }
                }
            }
        }


        private void recursiveDirProcessor(string dir)
        {

            string[] files = Directory.GetFiles(dir, "*.avi");
            //System.Windows.Forms.MessageBox.Show("Files found: " + files.Length.ToString(), "Message");

            foreach (string s in files)
            {
                if (File.Exists(s.Replace(".avi", ".youtube.url.txt")))
                {
                    string fileName = s.Replace(".avi", "").Replace("\\", "/");
                    fileName = fileName.Substring(fileName.LastIndexOf("/") + 1);
                    fileName = fileName.Substring(0, fileName.LastIndexOf("."));

                    string[] lines = File.ReadAllLines(s.Replace(".avi", ".youtube.url.txt"));

                    //UpdateYoutubeIdsForBvhFiles(fileName, lines[0]);
                    UpdatePathForBvhFiles(fileName, s.Replace(".avi", "")
                        .Replace("c:/Art/Projects/motion_capture_database/", "")
                        .Replace("\\", "/"));
                }
            }

            string[] dirs = Directory.GetDirectories(dir);
            // MessageBox.Show(dirs.Length.ToString());
            foreach (string s in dirs)
            {
                if (!s.Equals(".git"))
                {
                    recursiveDirProcessor(s);
                }
            }
        }

        public void InsertBvhCategory(string CategoryName, string CategoryID)
        {
            CategoryName = CategoryName.Replace(@"\", @"\\").Replace("'", @"\'");

            string query = "INSERT INTO BvhCategories (`CategoryName`, `CategoryID`) VALUES('" + CategoryName
                + "', " + CategoryID + ")";

            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }
        public void InsertBvhFile(string BvhCategoryId, string BvhName, string BvhDescr)
        {
            BvhName = BvhName.Replace(@"\", @"\\").Replace("'", @"\'");
            BvhDescr = BvhDescr.Replace(@"\", @"\\").Replace("'", @"\'");

            string query = "INSERT INTO BvhFiles (`BvhCategoryId`, `BvhName`, `BvhDescr`) VALUES(" + BvhCategoryId
                + ", '" + BvhName + "', '" + BvhDescr + "' )";

            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }
        public void UpdateYoutubeIdsForBvhFiles(string BvhName, string YoutubeId)
        {
            BvhName = BvhName.Replace(@"\", @"\\").Replace("'", @"\'");
            YoutubeId = YoutubeId.Replace(@"\", @"\\").Replace("'", @"\'");

            string query = "INSERT INTO BvhFiles ( BvhName, YoutubeId) VALUES( "
                + "'" + BvhName + "', " +
                "'" + YoutubeId + "' " +
                ") " +
                "ON DUPLICATE KEY UPDATE " +
                "YoutubeId = '" + YoutubeId + "' ";


            //create command and assign the query and connection from the constructor
            MySqlCommand cmd = new MySqlCommand(query, connection);

            //Execute command
            cmd.ExecuteNonQuery();
        }
        public void UpdatePathForBvhFiles(string BvhName, string pathToFile)
        {
            BvhName = BvhName.Replace(@"\", @"\\").Replace("'", @"\'");
            pathToFile = "/upload/bvh/" + pathToFile.Replace(@"\", @"\\").Replace("'", @"\'");

            string query = "INSERT INTO BvhFiles ( BvhName, YoutubeId) VALUES( "
                + "'" + BvhName + "', " +
                "'" + pathToFile + "' " +
                ") " +
                "ON DUPLICATE KEY UPDATE " +
                "pathToFile = '" + pathToFile + "' ";


            //create command and assign the query and connection from the constructor
            MySqlCommand cmd = new MySqlCommand(query, connection);

            //Execute command
            cmd.ExecuteNonQuery();
        }

        //open connection to database
        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        Console.WriteLine("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        Console.WriteLine("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        //Close connection
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
